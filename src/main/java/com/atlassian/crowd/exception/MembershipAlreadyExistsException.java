/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.exception;

/**
 * Thrown to indicate that a membership cannot be added because it already exists.
 *
 */
public class MembershipAlreadyExistsException extends ObjectAlreadyExistsException
{
    private final String childEntity;
    private final String parentEntity;

    public MembershipAlreadyExistsException(long directoryId, String childEntity, String parentEntity)
    {
        super("Membership already exists in directory [" + directoryId + "] from child entity [" + childEntity
              + "] to parent entity [" + parentEntity + "]");
        this.childEntity = childEntity;
        this.parentEntity = parentEntity;
    }

    public MembershipAlreadyExistsException(String childEntity, String parentEntity)
    {
        super("Membership already exists from child entity [" + childEntity + "] to parent entity ["
              + parentEntity + "]");
        this.childEntity = childEntity;
        this.parentEntity = parentEntity;
    }

    public String getChildEntity()
    {
        return childEntity;
    }

    public String getParentEntity()
    {
        return parentEntity;
    }
}
