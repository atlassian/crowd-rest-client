/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.model.event;

import com.atlassian.crowd.embedded.api.Directory;

import java.util.Collections;
import java.util.Set;

public class GroupMembershipEvent extends AbstractOperationEvent
{
    private final String groupName;
    private final Set<String> parentGroupNames;
    private final Set<String> childGroupNames;

    public GroupMembershipEvent(Operation operation, Directory directory, String groupName, String parentGroupName)
    {
        super(operation, directory);
        this.groupName = groupName;
        this.parentGroupNames = Collections.singleton(parentGroupName);
        this.childGroupNames = Collections.emptySet();
    }

    public GroupMembershipEvent(Operation operation, Directory directory, String groupName, Set<String> parentGroupNames, Set<String> childGroupNames)
    {
        super(operation, directory);
        this.groupName = groupName;
        this.parentGroupNames = parentGroupNames;
        this.childGroupNames = childGroupNames;
    }

    public String getGroupName()
    {
        return groupName;
    }

    public Set<String> getParentGroupNames()
    {
        return parentGroupNames;
    }

    public Set<String> getChildGroupNames()
    {
        return childGroupNames;
    }
}
