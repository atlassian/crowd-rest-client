/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.model.authentication;

import java.util.Date;

/**
 * A Session represents an authenticated, time-bound information exchange opportunity granted by a server to a client.
 * It is created at a certain point in time, and eventually expires, although its expiry date can
 * be renewed if the information exchange is alive.
 *
 * @version 2.6
 */
public interface Session
{
    /**
     * A session has an associated token that is granted by the server, and which can be presented by the client to
     * identify the session.
     * @return the identifier (key) of the token associated to the session.
     */
    String getToken();

    /**
     * @return the date when the session was created.
     */
    Date getCreatedDate();

    /**
     * @return the current expiry date after which the session will no longer be valid.
     */
    Date getExpiryDate();
}
