/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.model.user;

import com.atlassian.crowd.model.DirectoryEntity;

import java.security.Principal;

/**
 * Represents a user that exists in a directory.
 *
 * The order of the 'extends' statements is relevant, since {@link #getDirectoryId()} from {@link DirectoryEntity}
 * must shadow the deprecated method from {@link com.atlassian.crowd.embedded.api.User}.
 */
public interface User extends Principal, DirectoryEntity, com.atlassian.crowd.embedded.api.User
{
    String getFirstName();

    String getLastName();

    String getExternalId();

}
