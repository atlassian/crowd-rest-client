/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.integration.rest.entity;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * REST version of a validation factor (client-side).
 *
 * @since 2.7
 */
@XmlRootElement(name = "webhook")
@XmlAccessorType(XmlAccessType.FIELD)
public class WebhookEntity
{
    @XmlElement(name = "id")
    private final Long id;

    @XmlElement(name = "endpointUrl")
    private String endpointUrl;

    @XmlElement(name = "token")
    private String token;

    public WebhookEntity()
    {
        this.id = null;
    }

    public WebhookEntity(String endpointUrl, @Nullable String token)
    {
        this.id = null;
        this.endpointUrl = endpointUrl;
        this.token = token;
    }

    public long getId()
    {
        return id;
    }

    public String getEndpointUrl()
    {
        return endpointUrl;
    }

    public String getToken()
    {
        return token;
    }
}
